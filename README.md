# Script python -- generate-teapots.py #

Première expérience avec **python**.

Utilisation de BeautifulSoup : https://www.crummy.com/software/BeautifulSoup/bs4/doc/
Basé sur le code existant de https://github.com/brouberol/twenga-crawling

# Affichage du tableau #

Affichage des données en **Angular** en se basant sur un json généré par le script.

On peut voir les produits avec titre, image, prix, le stock et en cliquant sur "Afficher les données" une sidenav apparait avec les infos brutes.
Pour la redirection cliquer sur le produit.

Respect du angular styleguide : https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md

Utilisation de Brunch pour la compilation : http://brunch.io

Utilisation de angular material : https://material.angularjs.org/latest/

Pourrait etre optimisé en transformant les copier/coller en directives.

Pour compiler le tout juste à lancer npm run init. Brunch doit être installé ainsi que npm et toutes les dépendances du script python.

# En ligne #

Sur ce qui a en ligne il y a juste les fichiers compilés et le data.json crée par le script python.