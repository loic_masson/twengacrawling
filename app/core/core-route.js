(function(){
    'use strict';

    angular
        .module('app.core')
        .config(coreRoute);

    coreRoute.$inject = ['$stateProvider', '$urlRouterProvider'];

    function coreRoute($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/good-deal');

        $stateProvider
            .state('base', {
                abstract: true,
                views:{
                    'main':{
                        templateUrl: 'layout/main.html'
                    },
                    'header@base':{
                        templateUrl: 'layout/header.html'
                    },
                    'footer@base':{
                        templateUrl: 'layout/footer.html'
                    }
                }
            });

    }
})();