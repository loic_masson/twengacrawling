(function() {
    'use strict';

    angular
        .module('app.core', [
            'app.templates',
            'ui.router',
            'angular-loading-bar',
            'ngSanitize',
            'ngMaterial'
        ]);
})();