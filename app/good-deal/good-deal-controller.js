(function () {
    'use strict';

    angular
        .module("app.good-deal")
        .controller("GoodDealController", GoodDealController);

    GoodDealController.$inject = ['getTeapots', '$mdSidenav'];

    function GoodDealController(getTeapots, $mdSidenav) {
        var gdc = this;

        activate();

        function activate() {
            gdc.teapots = getTeapots;
            gdc.toggle = toggle;
            gdc.close = close;
            gdc.go = go;

        }

        function go(teapot){
            console.log("coucou");
            window.location.href= teapot.url;
        }

        function toggle(teapot, navId){
            gdc.current = teapot;
            $mdSidenav(navId).toggle();
        }

        function close(navId){
            $mdSidenav(navId).close();
        }


    }

})();


