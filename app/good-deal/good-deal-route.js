(function(){
    'use strict';

    angular
        .module('app.good-deal')
        .config(goodDealRoute);

    goodDealRoute.$inject = ['$stateProvider'];

    function goodDealRoute($stateProvider) {

        $stateProvider
            .state('base.good-deal', {
                url: "/good-deal",
                views:{
                    'content':{
                        templateUrl: 'good-deal/good-deal.html',
                        controller: "GoodDealController",
                        controllerAs: "gdc"
                    }
                },
                resolve: {
                    getTeapots: function($http){
                        function success(response){
                            return response.data;
                        }
                        return $http.get("data.json").then(success);
                    }
                }
            });
    }
})();