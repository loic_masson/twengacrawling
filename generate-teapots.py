# -*- coding: utf-8 -*-

""" Récupération du code de https://github.com/brouberol/twenga-crawling """
""" En le débuggant, en enlevant le superflu et en rajoutant la récupération des images """

import codecs
import re
import requests
import json

from urllib import urlretrieve

from gevent.pool import Pool
from bs4 import BeautifulSoup

def soupify_website(url):
    """ Retrieve webpage HTML code and return its 'soup' version. """
    try:
        import lxml
    except ImportError:
        return BeautifulSoup(open(urlretrieve(url)[0]))
    else:
        return BeautifulSoup(open(urlretrieve(url)[0]), "lxml")


def extract_products(soup):
    """ Extract the product list from the HTML soup using a CSS selector. """
    return soup.select('li.ctItem')


def title(soup):
	page_text = soup.select('.clr9 > span')[0].text
	title = BeautifulSoup(page_text, "html.parser")
	return title


def price(soup):
	page_text = soup.select('.price > span')[0].text
	price = BeautifulSoup(page_text, "html.parser")
	return price
	
def image(soup):
	image = soup.select('img')[0]['src']
	return image


def seller_url(soup):
    """ Extract the decoded seller URL from a product soup using
    a CSS selectior.
    Note: The extracted URLs need to be rot-13 decoded.
    """
    span = soup.select('span.a')[0]  # CSS selector
    uggc_url = span.attrs['data-erl']
    return uggc_url.decode('rot-13')  # return decoded url


def in_stock(url):
    """ Analyze the HTML code of the webpage of argument URL, in search
        of terms indicating that the object is (or not) in store.
        If terms indicating that the article is not in store are found,
        return False.
        If terms indicating that the article is in store are found,
        return True.
        If none of these terms are found, return True, as it's nothing
        unusual.
    """
    text = ' '.join([s.lower() for s in soupify_website(url).strings])

    unavailable = ['indisponible', 'plus disponible', 'approvisionnement']
    for term in unavailable:
        if term in text:
            return False

    available = ['en stock']
    for term in available:
        if term in text:
            return True
    return True


def product_features(product):
    """ Extract the title, price, seller URL and stock status of the
        argument product.
    """
    features = {}
    features['title'] = title(product)  # extract title
    features['price'] = price(product)  # extract price
    features['image'] = image(product)  # extract image
    features["url"] = seller_url(product)  # vendor url
    features['in_stock'] = in_stock(features['url'])  # availability
    print "Features extracted!"
    return features


def products_features(products):
    """ Perform the features extraction asynchronously. """
    pool = Pool(20)
    return pool.map(product_features, products)


def main():
    twenga_url = "http://twenga.fr/theiere.html"
    print "Extracting HTML data from %s..." % (twenga_url)
    soup = soupify_website(twenga_url)
    print "Scraping the 20 latest items..."
    teapots = extract_products(soup)[:20]
    print "Scraping products features..."
    features = products_features(teapots)
    print "Generating result json..."
    response = []
    for teapot in features:
		response.append({'title': teapot['title'].encode('utf-8'),'image': teapot['image'], 'price': teapot['price'].encode('utf-8'), 'url': teapot['url'],'stock': teapot['in_stock']})
    with open('./app/assets/data.json', 'w') as f:
     json.dump(response, f)    


if __name__ == '__main__':
    main()
