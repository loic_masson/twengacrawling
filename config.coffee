exports.config =
  conventions:
    ignored: [
      /[\\/]_/
      /vendor[\\/]node[\\/]/
      /vendor[\\/](j?ruby-.*|bundle)[\\/]/
      /.spec.js$/
    ]
  modules:
    definition: false
    wrapper: false
  files:
    javascripts:
      joinTo:
        'js/app.js': /^app/
        'js/vendor.js': /^(bower_components)/
      order:
        before: [
          'bower_components/jquery/dist/jquery.js'
          'bower_components/angular/angular.js'
          'bower_components/underscore/underscore.js'
          'bower_components/moment/src/moment.js',
          'app/core/core-module.js'
          'app/**/*-module.js'
          'app/app-module.js'
        ]
    stylesheets:
      joinTo:
        'style/app.css': 'app/core/general.scss'
        'style/vendor.css': /^(bower_components|vendor)/
      order:
        before:[
          'bower_components/bootstrap/dist/css/bootstrap.css'
        ]
    templates:
      joinTo:  'js/templates.js' : /^app/
  watcher:
    usePolling: true
  plugins:
    angular_templates:
      module: 'app.templates'
      path_transform: (path) -> path.replace('app/', '')
